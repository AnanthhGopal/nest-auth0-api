import { randomBytes } from 'crypto';
import { sign, SignOptions, verify } from 'jsonwebtoken';
import { Environment } from 'src/environment/environment';
import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import * as moment from 'moment';
import { Cache } from 'cache-manager';
var request = require("request");

@Injectable()
export class AuthenticationService {
    private readonly jwtOptions: SignOptions;
    private refreshTokenTtl: number;
    private expiresInDefault: number;
    constructor(
        @Inject(CACHE_MANAGER) private cacheManager: Cache) {
        // Get the access token expire time from Environment file.
        this.expiresInDefault = Environment.AccessTokenExpireSecond;
        // Assign the access token expire time into jwt.
        this.jwtOptions = { expiresIn: this.expiresInDefault };
        // Get the refresh token expire time from Environment file.
        this.refreshTokenTtl = Environment.RefreshTokenExpireSecond;
    }

    // Below method used to generate the access token by using jwt.
    async createAccessToken(userId: string): Promise<any> {
        // Get the Jwt formatted access token expire time.
        const options = this.jwtOptions;
        // Get the user details for generating the access token based on user details.
        const payload = {
            sub: userId
        };
        // Generate the access token Based on user details, jwt key, expire time.
        const signedPayload = sign(payload, Environment.JwtKey, options);
        // Remove the refresh token from cache memory.
        await this.cacheManager.del(userId);
        // Create a new refresh token.
        await this.createRefreshToken(userId);
        // Return the access token.
        return signedPayload;
    }

    // Below method used to generate the refresh token by using jwt.
    async createRefreshToken(userId: string): Promise<string> {
        // Generate the refresh token.
        const refreshToken = randomBytes(64).toString('hex');
        // Set the expires time for refresh token.
        let expiresAt = moment()
            .add(this.refreshTokenTtl, 's')
            .toDate();
        let request = {
            userId: userId,
            refreshToken: refreshToken,
            expiresAt: expiresAt
        }
        // Store refresh token into cache memory.
        await this.cacheManager.set(userId, request, { ttl: this.refreshTokenTtl });
        // Return the refresh token.
        return refreshToken;
    }

    // Below method used to validate the jwt token.
    async validateToken(token: string, ignoreExpiration: boolean = false): Promise<any> {
        return verify(
            token,
            Environment.JwtKey,
            {
                ignoreExpiration,
            }
        ) as {
            sub: string;
            iat?: number;
            exp?: number;
        };
    }

    async getAuthoAccessToken() {
        //TODO  Need to change below options from auth0 api application(you will get this details from auth0 website)
        var options = {
            method: 'POST',
            url: Environment.authOUrl,
            headers:
            {
                'content-type': 'application/json'
            },
            body: '{"client_id":"' + Environment.authOClientId + '","client_secret":"' + Environment.authOClientSecret + '","audience":"' + Environment.authOAudience + '","grant_type":"client_credentials"}'
        };
        return new Promise(function (resolve, reject) {
            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    reject(e);
                }
            })
        });
    }
}