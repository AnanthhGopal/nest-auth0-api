import { CACHE_MANAGER, ExecutionContext, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ExtractJwt } from 'passport-jwt';
import { Cache } from 'cache-manager';
import { AuthenticationService } from './authentication.service';
@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(
    private readonly authService: AuthenticationService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache
  ) {
    super();
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    // Get the request data from the service.
    const request = context.switchToHttp().getRequest();
    // Get the response data from the service.
    const response = context.switchToHttp().getResponse();
    // Get the authorization data from request Header.
    const accessToken = ExtractJwt.fromAuthHeaderAsBearerToken(['authorization'])(request);
    // Get the userId data from request Header.
    const userId = ExtractJwt.fromHeader('userid')(request);
    // check the access token is null or empty. if the token is null or empty, we return error.
    if (!accessToken) {
      throw new HttpException({ message: 'Access token is not set', errors: ["Access token is not set"] }, HttpStatus.BAD_REQUEST);
    }
    // Validate the access token is valid or not
    const isValidAccessToken = await this.authService.validateToken(accessToken, true);
    // check the access token details and expire time.
    if (isValidAccessToken && isValidAccessToken['sub'] && isValidAccessToken['exp']) {
      // check the access token user details and expire time.
      if (isValidAccessToken['sub'] != userId) {
        throw new HttpException({ message: 'Access token is not valid', errors: ["Access token is not valid"] }, HttpStatus.BAD_REQUEST);
      }
      // check the access token isExpire or not. if the token is valid and not expire we return true to the service request.
      if ((isValidAccessToken['exp'] * 1000) > new Date().getTime()) {
        return this.activate(context);
      }
    }

    // get the refresh token.
    const userRefreshToken = await this.cacheManager.get(userId);
    // check the refresh token details and expire time. if token not valid, we return the error.
    if (!userRefreshToken || !userRefreshToken["expiresAt"]) {
      throw new HttpException({ message: 'Refresh token not found', errors: ["Refresh token not found"] }, HttpStatus.BAD_REQUEST);
    }
    const currentDate = new Date();
    // check the access token expire time. if token expired , we return the error into the service
    if (new Date(userRefreshToken["expiresAt"]) < currentDate) {
      throw new HttpException({ message: 'Refresh token expired', errors: ["Refresh token expired"] }, HttpStatus.BAD_REQUEST);
    }
    // Create a new access token by using the user id.
    let newAccessToken = await this.authService.createAccessToken(userRefreshToken["userId"]);
    // append the new access token into header.
    response.set('authorization', newAccessToken);
    return this.activate(context);
  }

  async activate(context: ExecutionContext): Promise<boolean> {
    return super.canActivate(context) as Promise<boolean>;
  }

  handleRequest(err, user) {
    return user;
  }
}
