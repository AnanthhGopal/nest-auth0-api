import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { passportJwtSecret } from 'jwks-rsa';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    constructor() {
        //TODO  Need to change below configuration from auth0 api application(you will get this details from auth0 website)
        super({
            secretOrKeyProvider: passportJwtSecret({
                cache: true,
                rateLimit: true,
                jwksRequestsPerMinute: 5,
                jwksUri: 'https://dev-xcyqkpt1.us.auth0.com/.well-known/jwks.json',
            }),
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            audience: 'http://127.0.0.1:3651/',
            issuer: 'https://dev-xcyqkpt1.us.auth0.com/',
            algorithms: ['RS256'],
        });
    }

    validate(payload: any) {
        return payload;
    }
}