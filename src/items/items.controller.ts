import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { ItemsService } from './items.service';
import { AuthenticationService } from 'src/authz/authentication.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiQuery } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/authz/jwt-auth.guard';

@Controller('items')
export class ItemsController {
  constructor(private readonly itemsService: ItemsService,
    private readonly authService: AuthenticationService) { }

  @Get()
  @UseGuards(JwtAuthGuard)
  async findAll(): Promise<any> {
    return this.itemsService.findAll();
  }

  // below method used to generate the access token.
  @Get('token')
  @ApiQuery({ name: 'userId', type: 'string', required: true })
  async token(@Query('userId') userId: string): Promise<any> {
    return await this.authService.createAccessToken(userId);
  }

  // @Get('token')
  // async getAuth0Token() {
  //   //TODO  Need to change below options from auth0 api application(you will get this details from auth0 website)
  //   var options = {
  //     method: 'POST',
  //     url: 'https://dev-xcyqkpt1.us.auth0.com/oauth/token',
  //     headers:
  //     {
  //       'content-type': 'application/json'
  //     },
  //     body: '{"client_id":"zKFA91kxy7Kj5dy37yy8hzBFT0Z8u3oa","client_secret":"u-27Fp5i19YRM6NV3jrQrA4V8TsCicvKVJXONnR6ihJrVHxHtQ7QbqrAO_IUVxxp","audience":"http://127.0.0.1:3651/","grant_type":"client_credentials"}'
  //   };
  //   return new Promise(function (resolve, reject) {
  //     request(options, function (error, response, body) {
  //       if (error) throw new Error(error);
  //       try {
  //         resolve(JSON.parse(body));
  //       } catch (e) {
  //         reject(e);
  //       }
  //     })
  //   });
  // }
}