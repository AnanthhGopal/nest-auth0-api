import { Controller, Get, Request, Response, UseGuards, Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AppService } from './app.service';
import { googleClient } from './config';
import { ApiQuery } from '@nestjs/swagger';
const axios = require('axios');
@Controller('api')
export class AppController {
  constructor(private readonly service: AppService) { }
  // @Get("dragons")
  // @UseGuards(AuthGuard('jwt'))
  // async findAll(): Promise<any> {
  //   return [
  //     {
  //       "id": 1,
  //       "name": "Drogon",
  //       "source": "A Song of Ice and Fire"
  //     },
  //     {
  //       "id": 2,
  //       "name": "Viserion",
  //       "source": "A Song of Ice and Fire"
  //     },
  //     {
  //       "id": 3,
  //       "name": "Rhaegal",
  //       "source": "A Song of Ice and Fire"
  //     },
  //     {
  //       "id": 4,
  //       "name": "Falkor",
  //       "source": "The Neverending Story"
  //     },
  //     {
  //       "id": 5,
  //       "name": "Smaug",
  //       "source": "The Hobbit"
  //     },
  //     {
  //       "id": 6,
  //       "name": "Puff",
  //       "source": "Peter, Paul and Mary"
  //     }
  //   ];
  // }
  // @Get("google")
  // @UseGuards(AuthGuard('google'))
  // @ApiQuery({ name: 'inventoryId', type: 'number', required: false })
  // // eslint-disable-next-line @typescript-eslint/no-empty-function
  // googleAuth(@Query('inventoryId') inventoryId: string,) {
  // }

  // @Get('oauth2/callback')
  // @UseGuards(AuthGuard('google'))
  // googleAuthRedirect(@Request() req, @Response() res) {
  //   let response = this.service.googleLogin(req);
  //   res.redirect(googleClient.FRONT_END_URL + "?content=" + JSON.stringify(response));
  // }

  
}