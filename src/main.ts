import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.enableCors();
  app.useGlobalPipes(new ValidationPipe({
    disableErrorMessages: true,
  }));
  // This below code for enable the swagger ui
  const secondOptions = new DocumentBuilder()
    .setTitle("OAuth")
    .setDescription("OAuth")
    .setVersion("1.0")
    .build();
  const dogDocument = SwaggerModule.createDocument(app, secondOptions, {
    include: [],
  });
  SwaggerModule.setup('oauth', app, dogDocument);
  // This above code for enable the swagger ui

  await app.listen(3651);
  console.log(`Server is running at http://127.0.0.1:3651/oauth`);
}
bootstrap(); 