import { Strategy } from 'passport-google-oauth20';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, ExecutionContext } from '@nestjs/common';
import { googleClient } from './config';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      clientID: googleClient.clientID,
      clientSecret: googleClient.clientSecret,
      callbackURL: '/api/auth/oauth2/callback',
      scope: `profile email`
    });
  }
  handleRequest(err, user, info: Error) {
    const params = err;
  }
  async validate(accessToken, refreshToken, profile) {
    return {
      userId: profile.id,
      name: profile.displayName,
      username: profile.emails[0].value,
      picture: profile.photos[0].value,
      roles: ['user'],
      accessToken: accessToken
    };
  }
}
