import { CacheModule, Module } from '@nestjs/common';
import { ItemsService } from './items.service';
import { ItemsController } from './items.controller';
import { JwtModule } from '@nestjs/jwt';
import { AuthenticationService } from 'src/authz/authentication.service';

@Module({
  imports: [
    CacheModule.register(),
    JwtModule.register({
    }),
  ],
  providers: [ItemsService, AuthenticationService],
  controllers: [ItemsController],
})
export class ItemsModule { }